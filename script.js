document.addEventListener('DOMContentLoaded', () => {
    const amountInput = document.getElementById('amount');
    const fromCurrencySelect = document.getElementById('fromCurrency');
    const toCurrencySelect = document.getElementById('toCurrency');
    const convertBtn = document.getElementById('convertBtn');
    const resultDiv = document.getElementById('result');
// function for select options of form currency and To currency
    function selectCountry(){
        
        fetch('https://v6.exchangerate-api.com/v6/8c7f656c9adbeff4f84b3e68/latest/USD')
        .then((response)=> response.json())
        .then((country)=>{
// create a variable and store keys only from the object
        const fromCurrency = Object.keys(country.conversion_rates)
// to get all keys as seperate 
        for (let i = 0; i < fromCurrency.length; i++) {
            const currency = fromCurrency[i];
// create a option element 
            const option = document.createElement('option')
// change option text content and value
            option.value = currency
            option.textContent = currency
// appendChild to from currency and To currency option
            fromCurrencySelect.appendChild (option.cloneNode(true))
            toCurrencySelect.appendChild (option)
            console.log();           
        }
    })
    .catch((error)=> console.log(error))
}
// run eventlistner after clicking tte button
    convertBtn.addEventListener('click', getExchangerate)
//  function called by eventlistner
    function getExchangerate(){
 // variable to store the value of input and parseFloat is used to covert that to number
        const amount = parseFloat(amountInput.value);
        const fromCurrency = fromCurrencySelect.value
        const toCurrency = toCurrencySelect.value

// fetxh the api and append fromcurrency on it
        fetch(`https://v6.exchangerate-api.com/v6/8c7f656c9adbeff4f84b3e68/latest/${fromCurrency}`)
            .then((response)=> response.json())
            .then((exchangeRate)=>{
// checking amount input is empty
                if (amountInput.value == '') {
                    resultDiv.textContent = "Enter Amount";
                    return;
                }
//exchangeRate.conversion_rates to currency value stored in a variable(we type tocuuency value)
                const toExchange = exchangeRate.conversion_rates[toCurrency]
// condition check tocurrency available or not
                if (toExchange) {
// after that multiply amount value with api conversion rates
                    const convertedAmount = amount * toExchange
// display the exchangerates
                    resultDiv.textContent = `${amount} ${fromCurrency} = ${convertedAmount.toFixed(2)} ${toCurrency}`
                    console.log(resultDiv);
                } else {
                    resultDiv.textContent = "Exchange Rate is Not Available"
                }         
// After clicking amount field empty
                amountInput.value = '';
            })
            .catch((error)=> console.log(error))
}
// calling this function 
    selectCountry()

})


